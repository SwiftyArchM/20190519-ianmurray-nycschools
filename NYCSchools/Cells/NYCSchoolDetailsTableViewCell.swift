//
//  SchoolDetailsTableViewCell.swift
//  NYCSchools
//
//  Created by IAN MURRAY on 18/05/2019.
//  Copyright © 2019 IAN MURRAY. All rights reserved.
//

import UIKit

class NYCSchoolDetailsTableViewCell: UITableViewCell {
    
    @IBOutlet var overviewLabel: UILabel!
    
    static let reuseIdentifier = "OverviewCellReuseIdentifier"
}
