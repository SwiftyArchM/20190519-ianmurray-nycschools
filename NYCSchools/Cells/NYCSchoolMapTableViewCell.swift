//
//  NYCSchoolMapTableViewCell.swift
//  NYCSchools
//
//  Created by IAN MURRAY on 18/05/2019.
//  Copyright © 2019 IAN MURRAY. All rights reserved.
//

import UIKit
import MapKit

class NYCSchoolMapTableViewCell: UITableViewCell {

    @IBOutlet var mapView: MKMapView!
    
    static let reuseIdentifier = "MapCellReuseIdentifier"
    let regionRadius: CLLocationDistance = 500

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureWith(school: NYCSchool?) {
        if let latitude = school?.latitude, let longitude = school?.longitude {
            let initialLocation = CLLocation(latitude: latitude,
                                             longitude: longitude)
            self.centerMapOnLocation(location: initialLocation)
        }
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                                  latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }


}
