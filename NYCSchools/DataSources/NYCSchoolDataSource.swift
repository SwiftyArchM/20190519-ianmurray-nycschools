//
//  SchoolDataSource.swift
//  NYCSchools
//
//  Created by IAN MURRAY on 17/05/2019.
//  Copyright © 2019 IAN MURRAY. All rights reserved.
//

import UIKit

class NYCSchoolDataSource: NSObject, UITableViewDataSource {
    
    var schools = [NYCSchool]()
    let reuseId = "SchoolCellReuseIdentifier"
    
    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.schools.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseId, for: indexPath)
        let school = self.schools[indexPath.row]
        cell.textLabel?.text = school.schoolName
        return cell
    }
}
