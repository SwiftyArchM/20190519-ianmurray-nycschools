//
//  NYCSat.swift
//  NYCSchools
//
//  Created by IAN MURRAY on 17/05/2019.
//  Copyright © 2019 IAN MURRAY. All rights reserved.
//

import Foundation

struct NYCSat : Codable {
    var dbn: String
    var numberOfTestTakers : String
    var criticalReadingAverageScore: String
    var mathAverageScore: String
    var writingAverageScore: String
    var schoolName: String
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case numberOfTestTakers = "num_of_sat_test_takers"
        case criticalReadingAverageScore = "sat_critical_reading_avg_score"
        case mathAverageScore = "sat_math_avg_score"
        case writingAverageScore = "sat_writing_avg_score"
        case schoolName = "school_name"
    }
    
}
