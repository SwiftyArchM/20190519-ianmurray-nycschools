//
//  NYCSchool.swift
//  NYCSchools
//
//  Created by IAN MURRAY on 17/05/2019.
//  Copyright © 2019 IAN MURRAY. All rights reserved.
//

import Foundation

/// Struct represnting a school
struct NYCSchool : Comparable
{
    var schoolName: String
    var schoolEmail: String?
    var overview: String
    var website: String?
    var phoneNumber: String?
    var latitude: Double?
    var longitude: Double?
    
    // Convert keys to friendlier var names, aligned with Swift API Design guidelines
    
    enum CodingKeys: String, CodingKey {
        case schoolName = "school_name"
        case schoolEmail = "school_email"
        case overview = "overview_paragraph"
        case phoneNumber = "phone_number"
        
        case latitude
        case longitude
        case website
    }
    
    static func < (lhs: NYCSchool, rhs: NYCSchool) -> Bool {
        return lhs.schoolName < rhs.schoolName
    }
    
    static func == (lhs: NYCSchool, rhs: NYCSchool) -> Bool {
        return lhs.schoolName == rhs.schoolName
    }
}

// MARK: - Extensions
/// Provide a custom description
extension NYCSchool: CustomStringConvertible {
    var description: String {
        return "schoolName: \(schoolName) schoolEmail: \(String(describing: schoolEmail)), overviewParagraph: \(overview), website: \(String(describing: website)), phoneNumber: \(String(describing: phoneNumber)),latitude: \(String(describing: latitude)),longitude: \(String(describing: longitude))"
        
    }
}
/// Decode object and provide conversion for longitude and latitude
extension NYCSchool: Decodable {
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        schoolName = try values.decode(String.self, forKey: .schoolName)
        overview = try values.decode(String.self, forKey: .overview)

        schoolEmail = try? values.decode(String.self, forKey: .schoolEmail)
        website = try? values.decode(String.self, forKey: .website)
        phoneNumber = try? values.decode(String.self, forKey: .phoneNumber)
        
        if let latitudeString = try? values.decode(String.self, forKey: .latitude) {
            latitude = Double(latitudeString)
        }
        
        if let longitudeString = try? values.decode(String.self, forKey: .longitude) {
            longitude = Double(longitudeString)
        }
    }
}
