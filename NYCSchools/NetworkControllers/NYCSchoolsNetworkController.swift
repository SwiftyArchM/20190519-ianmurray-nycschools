//
//  NYCSchoolsInfoController.swift
//  NYCSchools
//
//  Created by IAN MURRAY on 17/05/2019.
//  Copyright © 2019 IAN MURRAY. All rights reserved.
//

import Foundation

// MARK: - API path builders
enum NYCSchoolsAPI {
    case allSchools
    case satResults
}

protocol Path {
    var path : String { get }
}

extension NYCSchoolsAPI : Path {
    var path: String {
        switch self {
        case .allSchools:
            return "/resource/s3k6-pzi2.json"
        case .satResults:
            return "/resource/f9bf-2cp4.json"
        }
    }
}

// MARK: - Error Handling
enum NYCSchoolError : Error {
    case NoData
    case ParseError
    case serverError
    case invalidURL
}

enum SchoolResult {
    case success([NYCSchool])
    case failure(Error)
    
    init(schools:[NYCSchool]) {
        self = .success(schools)
    }
    
    init(error:Error) {
        self = .failure(error)
    }
}

enum SATResult {
    case success(NYCSat?)
    case failure(Error)
    
    init(sat:NYCSat?) {
        self = .success(sat)
    }
    
    init(error:Error) {
        self = .failure(error)
    }
}


struct NYCSchoolsNetworkController
{
    let scheme = "https"
    let host = "data.cityofnewyork.us"
    
    func fetchAllNYCSchools(session:URLSession, completion: @escaping (SchoolResult) -> Void)
    {
        guard let schoolsURL = schoolsEndPoint(path: NYCSchoolsAPI.allSchools.path, queryItems: nil) else { return completion(SchoolResult(error: NYCSchoolError.invalidURL))}
        
        let task = session.dataTask(with: schoolsURL) { (data, response, error) in
            // Catch errors such as ATS and No Network
            if let error = error {
                completion(SchoolResult(error: error))
                return
            }
            
            // Catch server errors
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, let data = data else {
                completion(SchoolResult(error: NYCSchoolError.serverError))
                return
            }
            
            // Decode and catch any pasring errors
            do {
                let jsonDecoder = JSONDecoder()
                let nycSchools = try jsonDecoder.decode([NYCSchool].self, from: data)
                // Sort objects, allowable due to conformance of comparable
                completion(SchoolResult(schools: nycSchools.sorted()))
            }
            catch {
                completion(SchoolResult(error: NYCSchoolError.ParseError))
            }
        }
        task.resume()
    }
    
    func fetchSATForSchool(session:URLSession, school:String,  completion: @escaping (SATResult) -> Void)
    {
        guard let url = schoolsEndPoint(path: NYCSchoolsAPI.satResults.path,
                                        queryItems: ["school_name" : school.uppercased()]) else { return completion(SATResult(error: NYCSchoolError.invalidURL))}
        
        let task = session.dataTask(with: url) { (data, response, error) in
            // Catch errors such as ATS and No Network
            if let error = error {
                completion(SATResult(error: error))
                return
            }
            
            // Catch server errors
            guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, let data = data else {
                completion(SATResult(error: NYCSchoolError.serverError))
                return
            }
            
            // Decode and catch any pasring errors
            do {
                let jsonDecoder = JSONDecoder()
                let sats = try jsonDecoder.decode([NYCSat].self, from: data)
                completion(SATResult(sat: sats.first))
            }
            catch {
                completion(SATResult(error: NYCSchoolError.ParseError))
            }
        }
        task.resume()
    }
    
    
    func schoolsEndPoint (path: String, queryItems:[String : String]?) -> URL?
    {
        var components = URLComponents()
        components.scheme = scheme
        components.host = host
        components.path = path
        
        if let queryItems = queryItems {
            let queryItems : [URLQueryItem]? = queryItems.map {
                return URLQueryItem(name: $0, value: $1)
            }
            components.queryItems = queryItems
        }
        return components.url
    }
    
}
