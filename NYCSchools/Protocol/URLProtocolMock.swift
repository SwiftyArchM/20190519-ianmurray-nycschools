//
//  URLProtocolMock.swift
//  NYCSchools
//
//  Created by IAN MURRAY on 17/05/2019.
//  Copyright © 2019 IAN MURRAY. All rights reserved.
//

import UIKit

class URLProtocolMock: URLProtocol {
    
    static var testURLs = [URL? : Data]();
    
    override class func canInit(with request: URLRequest) -> Bool {
        return true
    }
    
    override class func canonicalRequest(for request: URLRequest) -> URLRequest {
        return request
    }
    
    override func startLoading() {
        if let url = request.url {
            if let data = URLProtocolMock.testURLs[url] {
                let response = HTTPURLResponse(url: url, statusCode: 200, httpVersion: "1.1", headerFields: nil)
                self.client?.urlProtocol(self, didReceive: response!, cacheStoragePolicy: .notAllowed)
                self.client?.urlProtocol(self, didLoad: data)
            }
            
            self.client?.urlProtocolDidFinishLoading(self)
        }
    }
    
    override func stopLoading() {}
    
}
