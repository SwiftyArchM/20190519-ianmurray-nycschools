//
//  NYCSchoolsDetailViewController.swift
//  NYCSchools
//
//  Created by IAN MURRAY on 17/05/2019.
//  Copyright © 2019 IAN MURRAY. All rights reserved.
//

import UIKit

class NYCSATResultsViewController: UIViewController {
    
    var school : NYCSchool?
    
    @IBOutlet var readingTextLabel: UILabel!
    @IBOutlet var readingResultsLabel: UILabel!
    
    @IBOutlet var writingTextLabel: UILabel!
    @IBOutlet var writingResultsLabel: UILabel!
    
    @IBOutlet var mathTextLabel: UILabel!
    @IBOutlet var mathResultsLabel: UILabel!
    
    @IBOutlet var noDataFoundLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideResults(true)
        self.noDataFoundLabel.isHidden = true

        let schoolsInfoController = NYCSchoolsNetworkController()
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        schoolsInfoController.fetchSATForSchool(session: URLSession.shared, school: school!.schoolName) { (result) in
            
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
            
            switch result {
            case .success(let fetchedSAT) :
                if let sat = fetchedSAT {
                    self.updateUI(with: sat)
                }
                else {
                    DispatchQueue.main.async { self.noDataFoundLabel.isHidden = false } 
                }
            case .failure(let error) :
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                DispatchQueue.main.async {
                    self.present(alert, animated: true, completion: nil)
                    self.noDataFoundLabel.isHidden = false
                }
            }
        }

    }
    func updateUI(with sat:NYCSat) {
        DispatchQueue.main.async {
            self.hideResults(false)
            self.readingResultsLabel.text = sat.criticalReadingAverageScore
            self.writingResultsLabel.text = sat.writingAverageScore
            self.mathResultsLabel.text = sat.mathAverageScore
        }
    }
    
    func hideResults(_ show : Bool) {
        self.readingTextLabel.isHidden = show
        self.writingTextLabel.isHidden = show
        self.mathTextLabel.isHidden = show
        self.readingResultsLabel.isHidden = show
        self.writingResultsLabel.isHidden = show
        self.mathResultsLabel.isHidden = show
    }
}
