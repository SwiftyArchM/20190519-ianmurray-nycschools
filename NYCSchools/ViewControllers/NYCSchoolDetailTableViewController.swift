//
//  NYCSchoolDetailTableViewController.swift
//  NYCSchools
//
//  Created by IAN MURRAY on 18/05/2019.
//  Copyright © 2019 IAN MURRAY. All rights reserved.
//

import UIKit

class NYCSchoolDetailTableViewController: UITableViewController {
    
    @IBOutlet var mailButton: UIButton!
    @IBOutlet var websiteButton: UIButton!
    @IBOutlet var callButton: UIButton!
    
    var school : NYCSchool?
    @IBOutlet var schoolNameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.schoolNameLabel.text = school!.schoolName
        websiteButton.isEnabled = ((school?.website) != nil)
        mailButton.isEnabled = ((school?.schoolEmail) != nil)
        callButton.isEnabled = ((school?.schoolEmail) != nil)
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: NYCSchoolDetailsTableViewCell.reuseIdentifier, for: indexPath) as? NYCSchoolDetailsTableViewCell  else {
            fatalError("The dequeued cell is not an instance of SchoolDetailsTableViewCell.")
        }
            cell.overviewLabel.text = school?.overview
            return cell
        }
        else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: NYCSchoolMapTableViewCell.reuseIdentifier, for: indexPath) as? NYCSchoolMapTableViewCell  else {
                fatalError("The dequeued cell is not an instance of NYCSchoolMapTableViewCell.")
            }
            cell.configureWith(school: school)
            return cell
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowSATResults" {
            if let navController = segue.destination as? UINavigationController,
                let schoolDetailViewController = navController.topViewController as? NYCSATResultsViewController {
                schoolDetailViewController.school = school
            }
        }
    }
    
    @IBAction func unwindToSchoolDetail(sender: UIStoryboardSegue) {}
    
    // MARK: - Actions

    @IBAction func mailButtonPressed(_ sender: Any) {
        if let email = school?.schoolEmail, let url = URL(string: "mailto:\(email)") {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func websiteButtonPressed(_ sender: Any) {
        if let website = school?.website, let url = URL(string: "http://" + website) {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func callButtonPressed(_ sender: Any) {
        if let tel = school?.phoneNumber, let url = URL(string: "tel://" + tel) {
            UIApplication.shared.open(url)
        }
    }
}
