//
//  NYCSchoolsTableViewController.swift
//  NYCSchools
//
//  Created by IAN MURRAY on 17/05/2019.
//  Copyright © 2019 IAN MURRAY. All rights reserved.
//

import UIKit

class NYCSchoolsTableViewController: UITableViewController {
    
    let schoolsInfoController = NYCSchoolsNetworkController()
    var schoolsDataSource = NYCSchoolDataSource()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    
        self.tableView.dataSource = schoolsDataSource
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        schoolsInfoController.fetchAllNYCSchools(session: URLSession.shared) { (result) in
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
            
            switch result {
            case .success(let fetchedSchools) :
                self.updateUI(with: fetchedSchools)
            case .failure(let error) :
                let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                DispatchQueue.main.async {
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    func updateUI(with schools:[NYCSchool]) {
        DispatchQueue.main.async {
            self.schoolsDataSource.schools = schools
            self.tableView.reloadData()
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowDetail" {
            let schoolDetailViewController = segue.destination as! NYCSchoolDetailTableViewController
            if let selectedSchoolCell = sender as? UITableViewCell {
                let indexPath = self.tableView.indexPath(for: selectedSchoolCell)!
                let selectedSchool = self.schoolsDataSource.schools[indexPath.row]
                schoolDetailViewController.school = selectedSchool
            }
        }
    }
    
}
