//
//  NYCSchoolsTests.swift
//  NYCSchoolsTests
//
//  Created by IAN MURRAY on 17/05/2019.
//  Copyright © 2019 IAN MURRAY. All rights reserved.
//

import XCTest

@testable import NYCSchools

class NYCSchoolsTests: XCTestCase {

    static let testBundle = Bundle(for: NYCSchoolsTests.self)
    
    let schoolsURL = testBundle.url(forResource: "Schools", withExtension: "json")!
    let malformedSchoolsURL = testBundle.url(forResource: "MalformedSchools", withExtension: "json")!
    
    let singleSAT = testBundle.url(forResource: "SingleSAT", withExtension: "json")!
    let malformedSingleSAT = testBundle.url(forResource: "MalformedSAT", withExtension: "json")!
    
    func JSONData(from url:URL) -> Data? {
        do {
            return try Data(contentsOf: url)
        } catch {
            return nil
        }
    }
    
    func testDecodingValidAllSchoolsData()
    {
        guard let stubbedData = JSONData(from: schoolsURL) else {
            XCTFail("Didn't get schools data")
            return
        }
        
        var schools : [NYCSchool]
        do {
            let jsonDecoder = JSONDecoder()
            let nycSchools = try jsonDecoder.decode([NYCSchool].self, from: stubbedData)
            schools = nycSchools
            XCTAssertFalse(schools.isEmpty, "'schools' should not be empty.")
            XCTAssertEqual(schools.count, 440)
            
        }
        catch {
            XCTFail("Unable to Parse")
        }
    }
    
    func testFetchValidAllSchoolsData () {
        let nycSchoolsInfoController = NYCSchoolsNetworkController()
        guard let stubbedData = JSONData(from: schoolsURL) else {
            XCTFail("Didn't get schools data")
            return
        }
        
        let url = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json")
        URLProtocolMock.testURLs = [url: Data(stubbedData)]
        
        let config = URLSessionConfiguration.ephemeral
        config.protocolClasses = [URLProtocolMock.self]
        let session = URLSession(configuration: config)
        
        let expectation = self.expectation(description: "Schools")
        nycSchoolsInfoController.fetchAllNYCSchools(session: session) { (result) in
            switch result {
            case .success(let fetchedSchools) :
                XCTAssertEqual(fetchedSchools.count, 440)
            case .failure( _) : break
            }
            expectation.fulfill()
        }
        waitForExpectations(timeout: 10, handler: nil)
    }
    
    func testFetchMalformedAllSchoolsData()
    {
        let nycSchoolsInfoController = NYCSchoolsNetworkController()

        guard let stubbedData = JSONData(from: malformedSchoolsURL) else {
            XCTFail("Didn't get schools data")
            return
        }
        let url = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json")
        URLProtocolMock.testURLs = [url: Data(stubbedData)]
        
        let config = URLSessionConfiguration.ephemeral
        config.protocolClasses = [URLProtocolMock.self]
        let session = URLSession(configuration: config)
        
        let expectation = self.expectation(description: "BadSchools")
        nycSchoolsInfoController.fetchAllNYCSchools(session: session) { (result) in
            switch result {
            case .success( _) :
                XCTFail("Wrong result")
            case .failure( _) : break
            }
            expectation.fulfill()
        }
        waitForExpectations(timeout: 10, handler: nil)
    }
    
    func testValidFetchSingleSAT()
    {
        let nycSchoolsInfoController = NYCSchoolsNetworkController()
        
        guard let stubbedData = JSONData(from: singleSAT) else {
            XCTFail("Didn't get SAT data")
            return
        }
        
        let schoolName = "EAST SIDE COMMUNITY SCHOOL"
        guard let url = nycSchoolsInfoController.schoolsEndPoint(path: NYCSchoolsAPI.satResults.path,
                                        queryItems: ["school_name" : schoolName.uppercased()])
            else { return  XCTFail("Didn't get a valid URL") }
        
        URLProtocolMock.testURLs = [url: Data(stubbedData)]        
        let config = URLSessionConfiguration.ephemeral
        config.protocolClasses = [URLProtocolMock.self]
        let ephemeralSession = URLSession(configuration: config)

        let expectation = self.expectation(description: "SingleSAT")
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        nycSchoolsInfoController.fetchSATForSchool(session: ephemeralSession, school: "EAST SIDE COMMUNITY SCHOOL") { (result) in
            switch result {
                case .success(let fetchedSAT) :
                    XCTAssertEqual(fetchedSAT?.schoolName, "EAST SIDE COMMUNITY SCHOOL")
                    XCTAssertEqual(fetchedSAT?.numberOfTestTakers, "70")
                    XCTAssertEqual(fetchedSAT?.criticalReadingAverageScore, "377")
                    XCTAssertEqual(fetchedSAT?.mathAverageScore, "402")
                    XCTAssertEqual(fetchedSAT?.writingAverageScore, "370")
                case .failure( _) : break
                }
            expectation.fulfill()
        }
        waitForExpectations(timeout: 10, handler: nil)
    }
    
    func testInvalidFetchSingleSAT()
    {
        let nycSchoolsInfoController = NYCSchoolsNetworkController()
        
        guard let stubbedData = JSONData(from: malformedSingleSAT) else {
            XCTFail("Didn't get SAT data")
            return
        }
        
        let schoolName = "EAST SIDE COMMUNITY SCHOOL"
        guard let url = nycSchoolsInfoController.schoolsEndPoint(path: NYCSchoolsAPI.satResults.path,
                                                                 queryItems: ["school_name" : schoolName.uppercased()])
            else { return  XCTFail("Didn't get a valid URL") }
        URLProtocolMock.testURLs = [url: Data(stubbedData)]
        
        let config = URLSessionConfiguration.ephemeral
        config.protocolClasses = [URLProtocolMock.self]
        let ephemeralSession = URLSession(configuration: config)
        
        let expectation = self.expectation(description: "SingleSAT")
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        nycSchoolsInfoController.fetchSATForSchool(session: ephemeralSession, school: "EAST SIDE COMMUNITY SCHOOL") { (result) in
            
            switch result {
            case .success( _) :
                XCTFail("Wrong result")
            case .failure( _) : break
            }
            expectation.fulfill()
        }
        waitForExpectations(timeout: 10, handler: nil)
    }
}
