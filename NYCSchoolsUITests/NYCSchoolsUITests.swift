//
//  NYCSchoolsUITests.swift
//  NYCSchoolsUITests
//
//  Created by IAN MURRAY on 17/05/2019.
//  Copyright © 2019 IAN MURRAY. All rights reserved.
//

import XCTest

class NYCSchoolsUITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testSchools() {
        
        let app = XCUIApplication()
        let tablesQuery = app.tables
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Academy for Conservation and the Environment"]/*[[".cells.staticTexts[\"Academy for Conservation and the Environment\"]",".staticTexts[\"Academy for Conservation and the Environment\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.swipeUp()
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["Academy for Environmental Leadership"]/*[[".cells.staticTexts[\"Academy for Environmental Leadership\"]",".staticTexts[\"Academy for Environmental Leadership\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app.navigationBars["NYCSchools.NYCSchoolDetailTableView"].buttons["SAT Results"].tap()
        app.navigationBars["SAT Results"].buttons["Cancel"].tap()
        
    }
    
    func testMail() {
        let app = XCUIApplication()
        let tablesQuery = app.tables
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["A. Philip Randolph Campus High School"]/*[[".cells.staticTexts[\"A. Philip Randolph Campus High School\"]",".staticTexts[\"A. Philip Randolph Campus High School\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        tablesQuery.buttons["Mail"].tap()        
    }
    
    func testWebsite() {
        let app = XCUIApplication()
        let tablesQuery = app.tables
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["A. Philip Randolph Campus High School"]/*[[".cells.staticTexts[\"A. Philip Randolph Campus High School\"]",".staticTexts[\"A. Philip Randolph Campus High School\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        tablesQuery.buttons["Website"].tap()
    }
    
    func testCall() {
        let app = XCUIApplication()
        let tablesQuery = app.tables
        tablesQuery/*@START_MENU_TOKEN@*/.staticTexts["A. Philip Randolph Campus High School"]/*[[".cells.staticTexts[\"A. Philip Randolph Campus High School\"]",".staticTexts[\"A. Philip Randolph Campus High School\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        tablesQuery.buttons["Call"].tap()
    }
    
    

}
